EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
EELAYER 25 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 1
Title "USB Micro-B to B PCB Adapter"
Date "2018-07-16"
Rev "1.0"
Comp "Scott Alfter"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L USB_B J1
U 1 1 5B4D53C2
P 5100 3800
F 0 "J1" H 4900 4250 50  0000 L CNN
F 1 "USB_B" H 4900 4150 50  0000 L CNN
F 2 "Connectors:USB_B" H 5250 3750 50  0001 C CNN
F 3 "" H 5250 3750 50  0001 C CNN
	1    5100 3800
	1    0    0    -1  
$EndComp
$Comp
L USB_OTG J2
U 1 1 5B4D53FF
P 6450 3800
F 0 "J2" H 6250 4250 50  0000 L CNN
F 1 "USB_OTG" H 6250 4150 50  0000 L CNN
F 2 "Connectors_USB:USB_Micro-B_Molex_47346-0001" H 6600 3750 50  0001 C CNN
F 3 "" H 6600 3750 50  0001 C CNN
	1    6450 3800
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5400 3800 6150 3800
Wire Wire Line
	5400 3900 6150 3900
Wire Wire Line
	5100 4200 6450 4200
Wire Wire Line
	5000 4200 5000 4250
Wire Wire Line
	5000 4250 6550 4250
$Comp
L Jumper JP1
U 1 1 5B4D54EA
P 5750 3600
F 0 "JP1" H 5750 3750 50  0000 C CNN
F 1 "Jumper" H 5750 3520 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" H 5750 3600 50  0001 C CNN
F 3 "" H 5750 3600 50  0001 C CNN
	1    5750 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 3600 5450 3600
Wire Wire Line
	6050 3600 6150 3600
Wire Wire Line
	6550 4250 6550 4200
NoConn ~ 6150 4000
$EndSCHEMATC
